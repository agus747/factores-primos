<?php
include "FactoresPrimos.php";

$numero = 600851475143;

$factores_primos = new FactoresPrimos;


$factores = $factores_primos->obtenerFactoresPrimos($numero);

echo "El factor primo más grande del número {$numero} es {$factores[count($factores) - 1]}\n";

