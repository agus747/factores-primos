<?php

class FactoresPrimos {

    /**
     * Devuelve un array con los factores primos del número dado
     * 
     */
    public function obtenerFactoresPrimos($numero) 
    {
        $factores_primos =[];
        $factor = 1;
        $i = 0;

        /**
         * Divido el número hasta llegar a 1, voy calculando factor por factor para ahorrar procesamiento
         */
        while ($numero > 1) {                        
            
            $factor = $this->proximoFactorPrimo($factor,$numero);            
            
            while (($numero % $factor) == 0) {                
                $numero = $numero / $factor;
                $factores_primos[] = $factor;
            }            

        }

        return $factores_primos;
    }

    
    /**
     * Devuelve el factor primo que le sigue a el factor anterior del número
     * 
     */
    public function proximoFactorPrimo($factorAnterior, $numero)
    {
        $esFactorPrimo = false;
        $posibleFactor = $factorAnterior;

        do {
            $posibleFactor++;        
            if ( ($numero % $posibleFactor) == 0 ) {
                $esFactorPrimo = $this->esPrimo($posibleFactor);
            }                
        } while (!$esFactorPrimo);

        return $posibleFactor;
    }

    /**
     * Evalua si el número dado es primo
     */
    protected function esPrimo($numero)
    {
        $i = 2;
        $esPrimo = true;

        while ($esPrimo && $i < $numero ) {
            $esPrimo = ($numero % $i) != 0;
            $i++;            
        }

        return $esPrimo;
    }

}